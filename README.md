# Process State Trace 💻
Welcome to a Web app where simulates process scheduling by using Round Robin algorithm for the Operating system course at UFPS 2023-2.

# Interaction with the page.
Here are a series of steps for you to have an incredible experience with this simulator.


1. Enter the number of dispatcher cycles and the number of instruction cycles (quantum).
2. Add the processes you want by clicking the "Add information Process" button.
>**TIP:**
>Remember that you need to have the basic concepts of a process, such as: It cannot start or end with an interruption, memory locations must be sequential and non-repeating, among others.
3. Click the "Execute scheduler" button to see the process tracking below.
4. If you entered an interrupt instruction, you can choose whether or not to end the interruption by clicking the "wake up" button. Alternatively, you can continue the process execution by clicking the "green arrow" ➡️ button.
5. As you run the processes, you can see below in which queue the process is located, whether it's in the "ready," "blocked," or "running" queue, and its current state.
6. You can also view the "Trace processor" in its entirety continuously. It provides the same information as the table with the state process.

>**NOTE:**
Here, we provide you with a table to help you understand the meaning of each entry and the meaning of each color of the process state.
![Instructions table](instructions.PNG)
![Process State Table](processState.PNG)

# Team Members
- Jhoan Sebastian Perez Acosta
- Orlando Jose Beltran Valero
